# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is for those who want to download a clean episerver repo.
Different versions will be provided in different branches.

This is a version where connect the local DB through SQL Server. Not because this is the best but this will be more like a real site.
We also add the site into IIS.

### How do I get set up? ###

You will need:
- SQL Server
- SQL Server Management Studio
- Visual Studio 2017
- Internet Information Service (IIS)

- Create user if you don't have one in Windows 10. This user is for login to CMS.
	- Control panel
	- User Accounts
	- Manage another account
	- Add a new user in PC settings
	- Add someone else to this PC.
	- Search after text "I don't have this person's sign-in information" and click it.
	- Search after text "Add a user without a Microsoft account" and click it.
	- Create user and save the information.
- Install IIS if you haven't already.
	- Control panel
	- Programs
	- Turn Windows features on or off
	- Search for "Internet Information Service" and check in the checkbox
	- Search for "Application Development Features" under "World Wide Web Services".
	- Check al checkboxes but ".NET Extensibility 3.5", "ASP.NET 3.5" and "CGI". You will not need it.
	- Press "OK"
- Download reop if you haven't already.
- Attach database in SQL.
	- Open SQL Server Management Studio
	- Select "New Query"
	- Add this:
	CREATE DATABASE DatabaseName 
		ON (FILENAME = 'FilePath\FileName.mdf'), -- Main Data File .mdf
		(FILENAME = 'FilePath\LogFileName.ldf'), -- Log file .ldf
		 (FILENAME = 'FilePath\SecondaryDataFile.ndf)  -- Optional - any secondary data files
		FOR ATTACH 
	GO 

### Contribution guidelines ###
No at the moment. 

### Who do I talk to? ###
Daniel Gustafsson
daniel.gustafsson@ffcg.se